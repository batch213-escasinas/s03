<?php 

class PersonName {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }
    public function printName(){
        return "Your full name is $this->firstName $this->middleName $this->lastName";
    }
};

class DeveloperNAME extends PersonName{
    public function printName(){
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
    }
};

class EngineerName extends PersonName{
    public function printName(){
        return "You are an engineer named $this->firstName $this->middleName $this->lastName";
    }
};

$person = new PersonName('John', '', 'Smith');
$developer = new DeveloperName('Jane', 'Doe', 'Smith');
$engineer = new EngineerName('Macky', 'Tamayo', 'Escasinas');
?>